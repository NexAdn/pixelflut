#include <atomic>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <unistd.h>

#define WITH_PNG

namespace
{
constexpr const char* SERVER{"2a01:4f8:222:6d0::2"};
// constexpr const char* SERVER{"151.217.176.193"};
// constexpr const char* SERVER{"127.0.0.1"};
constexpr short SERVER_PORT{1234};
constexpr int NUM_THREADS{8};
constexpr int OFFSET_X{500};
constexpr int OFFSET_Y{500};
constexpr int MAX_WAIT{64};
constexpr int RETRY_CHECK_INTERVAL{10};

size_t n_instructions{0};
std::atomic<size_t> last_sent_instructions{0};
std::atomic<size_t> open_connections{0};
std::atomic<size_t> pending_connections{0};
} // namespace

void restart_threads(const std::string& msg, struct sockaddr_in6& server,
                     std::vector<std::thread>& thread_pool, size_t threads_to_start = NUM_THREADS)
{
	for (int j = 0; j < threads_to_start; j++)
	{
		thread_pool.push_back(
		  std::thread([j, &msg, &server, &n_instructions, &last_sent_instructions]() {
			  while (true)
			  {
				  pending_connections++;
				  std::this_thread::sleep_for(std::chrono::seconds(j % MAX_WAIT));
				  int sockfd = socket(AF_INET6, SOCK_STREAM, 0);
				  if (sockfd < 0)
				  {
					  std::cerr << "socket() failed\n";
					  pending_connections--;
					  continue;
				  }
				  int res = connect(sockfd, (struct sockaddr*) &server, sizeof(server));
				  if (res < 0)
				  {
					  std::cerr << "connect() failed\n";
					  close(sockfd);
					  pending_connections--;
					  continue;
				  }
				  pending_connections--;
				  open_connections++;

				  while (res >= 0)
				  {
					  res = send(sockfd, msg.c_str(), msg.size(), MSG_NOSIGNAL);
					  if (res < 0)
					  {
						  std::clog << "send() failed\n";
						  close(sockfd);
						  open_connections--;
						  break;
					  }
					  else
					  {
						  last_sent_instructions += n_instructions;
					  }
				  }
			  }
		  }));
	}
}

int main(int argc, char** argv)
{
	struct sockaddr_in6 server
	{
		.sin6_family = AF_INET6, .sin6_port = htons(SERVER_PORT)
	};
	inet_pton(AF_INET6, SERVER, &server.sin6_addr);

	int sockfd = socket(AF_INET6, SOCK_STREAM, 0);
	if (sockfd < 0)
	{
		std::cerr << "socket() failed\n";
		return 1;
	}

	std::cout << "Connecting\n";
	int res = connect(sockfd, (struct sockaddr*) &server, sizeof(server));
	if (res < 0)
	{
		std::cerr << "connect() failed\n";
		std::cerr << "\terrno: " << errno << std::endl;
		return 1;
	}
	std::cout << "Connected\n";

	int x_size, y_size;
	const char* read_size_msg = "SIZE\n";
	write(sockfd, read_size_msg, sizeof(read_size_msg));
	char buf[64];
	read(sockfd, buf, sizeof(buf));
	std::istringstream size_ss(buf);
	std::string throwaway;
	size_ss >> throwaway;
	size_ss >> x_size >> y_size;

	std::cout << "Size is " << x_size << "x" << y_size << std::endl;

	close(sockfd);

#ifdef WITH_PNG
	if (argc < 2)
	{
		std::cerr << "No px file supplied\n";
		return 1;
	}

	std::ifstream in_file(argv[1]);
	std::stringstream msg_buf;
	std::string line;
	msg_buf << "OFFSET " << OFFSET_X << ' ' << OFFSET_Y << '\n';
	while (std::getline(in_file, line))
	{
		msg_buf << line << '\n';
		n_instructions++;
	}
#else
	uint8_t r{0};
	uint8_t g{0};
	uint8_t b{0};

	std::ostringstream msg_buf;
	for (int i = 0; i < x_size * 10; i++)
	{
		for (int j = 0; j < y_size * 10; j++)
		{
			char buf[64];
			sprintf(buf, "PX %d %d %02x%02x%02x\n", rand() % x_size, rand() % y_size, r++, g, b);
			msg_buf << buf;

			if (r == 0)
				g++;

			if (g == 0)
				b++;

			n_instructions++;
		}
	}
#endif

	std::cerr << "Message has " << n_instructions << " instructions\n";

	std::string msg{msg_buf.str()};

	std::vector<std::thread> threads;

	size_t last_instructions_ema{0};

	restart_threads(msg, server, threads);

	int retry_check{0};

	while (true)
	{
		size_t last_instrs{last_sent_instructions};
		last_sent_instructions = 0;
		if (last_instructions_ema == 0)
		{
			last_instructions_ema = last_instrs;
		}
		else
		{
			last_instructions_ema = 0.05 * last_instrs + 0.95 * last_instructions_ema;
		}
		std::this_thread::sleep_for(std::chrono::seconds(1));
		// std::cout << "Sent " << last_instrs << " instructions \tEMA: " << last_instructions_ema
		//          << "\n";
		std::cout << "P: " << pending_connections << "\tC: " << open_connections
		          << "\tEMA: " << last_instructions_ema << "\tLT: " << last_instrs << "\n";
	}

	for (auto& t : threads)
	{
		if (t.joinable())
			t.join();
	}

	return 0;
}
