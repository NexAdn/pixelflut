#include <cmath>
#include <cstdio>
#include <deque>
#include <fstream>
#include <iostream>
#include <string>

#include <png++/png.hpp>

int main(int argc, char** argv)
{
	if (argc != 3)
	{
		std::cerr << "Invalid arguments\n"
			<< "Syntax: pngtopx [SRC] [DST]\n";
		return 1;
	}

	std::ostream* out{&std::cout};
	png::image<png::rgba_pixel>* img;

	if (std::string(argv[1]) == "-")
	{
		img = new png::image<png::rgba_pixel>(std::cin);
	}
	else
	{
		img = new png::image<png::rgba_pixel>(argv[1]);
	}

	if (std::string(argv[2]) != "-")
	{
		std::ofstream* out_file = new std::ofstream(argv[2], std::ios::trunc);
		if (out_file->bad())
		{
			std::cerr << "Failed to open output file\n";
			return 1;
		}
		out = out_file;
	}

	std::deque<std::string> px_messages;
	auto width = img->get_width();
	auto height = img->get_height();
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			png::rgba_pixel px = img->get_pixel(i, j);
			if (px.alpha == 0x00)
				continue;

			char msg[32];
			if (px.alpha != 0xff)
			{
				sprintf(msg, "PX %d %d %02x%02x%02x%02x\n", i, j, px.red, px.green, px.blue,
				        px.alpha);
			}
			else
			{
				sprintf(msg, "PX %d %d %02x%02x%02x\n", i, j, px.red, px.green, px.blue);
			}
			if (rand() % 2 == 0)
			{
				px_messages.push_front(std::string(msg));
			}
			else
			{
				px_messages.push_back(std::string(msg));
			}
		}
	}

	for (auto& msg : px_messages)
		*out << msg;

	if (out != &std::cout)
	{
		auto* out_file = dynamic_cast<std::ofstream*>(out);
		out_file->close();
		delete out_file;
	}

	delete img;

	return 0;
}
