#include <iostream>
#include <queue>
#include <sstream>
#include <string>

class IPXStream
{
public:
	IPXStream(unsigned int scale = 1, uint8_t r = 255, uint8_t g = 255, uint8_t b = 255)
	    : scale(scale), r(r), g(g), b(b)
	{
	}

	~IPXStream()
	{
	}

	static size_t get_x_size(char c)
	{
		switch (c)
		{
		default:
			return 0;
		}
	}

	static size_t get_y_size(char c)
	{
		switch (c)
		{
		default:
			return 0;
		}
	}

	inline std::ostringstream& px_message(std::ostringstream& buf, int i, int j)
	{
		// buf << "PX " << (x_off * scale) + i << ' ' << (y_off * scale) + j << ' ' << std::hex
		// << static_cast<int>(r) << ' ' << static_cast<int>(g) << ' ' << static_cast<int>(b) <<
		// '\n';
		char char_buf[64];
		sprintf(char_buf, "PX %d %d %02x%02x%02x\n", static_cast<int>((x_off * scale) + i),
		        static_cast<int>((y_off * scale) + j), r, g, b);
		buf << char_buf;
		return buf;
	}

	std::ostringstream& right_stroke(std::ostringstream& buf, size_t len)
	{
		for (int i = 0; i < scale * len; i++)
		{
			for (int j = 0; j < scale; j++)
			{
				px_message(buf, i, j);
			}
		}
		return buf;
	}

	std::ostringstream& down_stroke(std::ostringstream& buf, size_t len)
	{
		for (size_t i = 0; i < scale; i++)
		{
			for (size_t j = 0; j < scale * len; j++)
			{
				px_message(buf, i, j);
			}
		}
		return buf;
	}

	std::string get_px_message(char c)
	{
		std::ostringstream buf;
		switch (c)
		{
		case 'A':
		case 'a':
			down_stroke(buf, 5);
			right_stroke(buf, 4);
			y_off += 2;
			right_stroke(buf, 4);
			y_off -= 2;
			x_off += 3;
			down_stroke(buf, 5);
			x_off++;
			break;
		case 'B':
		case 'b':
			down_stroke(buf, 5);
			right_stroke(buf, 3);
			y_off += 2;
			right_stroke(buf, 3);
			y_off += 2;
			right_stroke(buf, 3);
			y_off -= 3;
			x_off += 3;
			down_stroke(buf, 1);
			y_off += 2;
			down_stroke(buf, 1);
			y_off -= 3;
			x_off++;
			break;

		case 'C':
		case 'c':
			x_off++;
			right_stroke(buf, 2);
			x_off--;
			y_off++;
			down_stroke(buf, 3);
			y_off += 3;
			x_off++;
			right_stroke(buf, 2);
			x_off += 2;
			y_off--;
			right_stroke(buf, 1);
			y_off -= 2;
			right_stroke(buf, 1);
			y_off--;
			x_off++;
			break;

		case 'D':
		case 'd':
			right_stroke(buf, 3);
			down_stroke(buf, 5);
			y_off += 4;
			right_stroke(buf, 3);
			x_off += 3;
			y_off -= 3;
			down_stroke(buf, 3);
			y_off--;
			x_off++;
			break;

		case 'E':
		case 'e':
			right_stroke(buf, 4);
			down_stroke(buf, 5);
			y_off += 2;
			right_stroke(buf, 3);
			y_off += 2;
			right_stroke(buf, 4);
			y_off -= 4;
			x_off += 4;
			break;

		case 'F':
		case 'f':
			right_stroke(buf, 4);
			down_stroke(buf, 5);
			y_off += 2;
			right_stroke(buf, 3);
			y_off -= 2;
			x_off += 4;
			break;

		case 'G':
		case 'g':
			x_off++;
			right_stroke(buf, 3);
			x_off--;
			y_off++;
			down_stroke(buf, 3);
			y_off += 3;
			x_off++;
			right_stroke(buf, 2);
			x_off++;
			y_off -= 2;
			right_stroke(buf, 2);
			x_off++;
			down_stroke(buf, 2);
			x_off++;
			y_off -= 2;
			break;

		case 'H':
		case 'h':
			down_stroke(buf, 5);
			x_off += 3;
			down_stroke(buf, 5);
			x_off -= 3;
			y_off += 2;
			right_stroke(buf, 4);
			y_off -= 2;
			x_off += 4;
			break;

		case 'I':
		case 'i':
			right_stroke(buf, 3);
			y_off += 4;
			right_stroke(buf, 3);
			y_off -= 4;
			x_off++;
			down_stroke(buf, 5);
			x_off += 2;
			break;

		case 'J':
		case 'j':
			right_stroke(buf, 4);
			x_off += 3;
			down_stroke(buf, 4);
			x_off -= 2;
			y_off += 4;
			right_stroke(buf, 2);
			y_off--;
			x_off--;
			right_stroke(buf, 1);
			x_off += 4;
			y_off -= 3;
			break;

		case 'K':
		case 'k':
			down_stroke(buf, 5);
			y_off += 1;
			x_off += 2;
			right_stroke(buf, 1);
			y_off += 2;
			right_stroke(buf, 1);
			y_off -= 3;
			x_off++;
			right_stroke(buf, 1);
			y_off += 4;
			right_stroke(buf, 1);
			y_off -= 2;
			x_off -= 2;
			right_stroke(buf, 1);
			y_off -= 2;
			x_off += 3;
			break;

		case 'L':
		case 'l':
			down_stroke(buf, 5);
			y_off += 4;
			right_stroke(buf, 3);
			y_off -= 4;
			x_off += 3;
			break;

		case 'M':
		case 'm':
			down_stroke(buf, 5);
			x_off++;
			y_off++;
			right_stroke(buf, 1);
			x_off++;
			y_off++;
			right_stroke(buf, 1);
			x_off++;
			y_off--;
			right_stroke(buf, 1);
			x_off++;
			y_off--;
			down_stroke(buf, 5);
			x_off++;
			break;

		case 'N':
		case 'n':
			down_stroke(buf, 5);
			x_off++;
			y_off++;
			right_stroke(buf, 1);
			x_off++;
			y_off++;
			right_stroke(buf, 1);
			x_off++;
			y_off++;
			right_stroke(buf, 1);
			x_off++;
			y_off -= 3;
			down_stroke(buf, 5);
			x_off++;
			break;

		case 'O':
		case 'o':
			x_off++;
			right_stroke(buf, 2);
			x_off--;
			y_off++;
			down_stroke(buf, 3);
			y_off += 3;
			x_off++;
			right_stroke(buf, 2);
			y_off -= 3;
			x_off += 2;
			down_stroke(buf, 3);
			y_off--;
			x_off++;
			break;

		case 'P':
		case 'p':
			down_stroke(buf, 5);
			right_stroke(buf, 3);
			y_off += 2;
			right_stroke(buf, 3);
			y_off--;
			x_off += 3;
			down_stroke(buf, 1);
			y_off--;
			x_off++;
			break;

		case 'Q':
		case 'q':
			x_off++;
			right_stroke(buf, 2);
			x_off--;
			y_off++;
			down_stroke(buf, 3);
			y_off += 3;
			x_off++;
			right_stroke(buf, 2);
			y_off -= 3;
			x_off += 2;
			down_stroke(buf, 3);
			x_off--;
			y_off += 2;
			right_stroke(buf, 1);
			x_off++;
			y_off++;
			right_stroke(buf, 1);
			y_off -= 4;
			x_off++;
			break;

		case 'R':
		case 'r':
			down_stroke(buf, 5);
			x_off++;
			right_stroke(buf, 2);
			x_off += 2;
			y_off++;
			right_stroke(buf, 1);
			x_off -= 2;
			y_off++;
			right_stroke(buf, 2);
			x_off++;
			y_off++;
			right_stroke(buf, 1);
			x_off++;
			y_off++;
			right_stroke(buf, 1);
			y_off -= 4;
			x_off++;
			break;

		case 'S':
		case 's':
			x_off++;
			right_stroke(buf, 3);
			x_off--;
			y_off++;
			right_stroke(buf, 1);
			y_off++;
			x_off++;
			right_stroke(buf, 2);
			x_off += 2;
			y_off++;
			right_stroke(buf, 1);
			y_off++;
			x_off -= 3;
			right_stroke(buf, 3);
			x_off += 4;
			y_off -= 4;
			break;

		case 'T':
		case 't':
			right_stroke(buf, 5);
			x_off += 2;
			down_stroke(buf, 5);
			x_off += 3;
			break;

		case 'U':
		case 'u':
			down_stroke(buf, 4);
			x_off++;
			y_off += 4;
			right_stroke(buf, 2);
			x_off += 2;
			y_off -= 4;
			down_stroke(buf, 4);
			x_off++;
			break;

		case 'V':
		case 'v':
			down_stroke(buf, 2);
			x_off++;
			y_off += 2;
			down_stroke(buf, 2);
			x_off++;
			y_off += 2;
			right_stroke(buf, 1);
			x_off++;
			y_off -= 2;
			down_stroke(buf, 2);
			x_off++;
			y_off -= 2;
			down_stroke(buf, 2);
			x_off++;
			break;

		case 'W':
		case 'w':
			down_stroke(buf, 2);
			x_off++;
			y_off += 2;
			down_stroke(buf, 2);
			x_off++;
			y_off += 2;
			down_stroke(buf, 1);
			x_off++;
			y_off -= 2;
			down_stroke(buf, 2);
			x_off++;
			y_off += 2;
			down_stroke(buf, 1);
			x_off++;
			y_off -= 2;
			down_stroke(buf, 2);
			x_off++;
			y_off -= 2;
			down_stroke(buf, 2);
			x_off++;
			break;

		case 'X':
		case 'x':
			down_stroke(buf, 1);
			x_off++;
			y_off++;
			down_stroke(buf, 1);
			x_off++;
			y_off++;
			down_stroke(buf, 1);
			x_off++;
			y_off++;
			down_stroke(buf, 1);
			x_off++;
			y_off++;
			down_stroke(buf, 1);
			y_off -= 4;
			down_stroke(buf, 1);
			x_off--;
			y_off++;
			down_stroke(buf, 1);
			x_off--;
			y_off++;
			down_stroke(buf, 1);
			x_off--;
			y_off++;
			down_stroke(buf, 1);
			x_off--;
			y_off++;
			down_stroke(buf, 1);
			x_off += 5;
			y_off -= 4;
			break;

		case 'Y':
		case 'y':
			down_stroke(buf, 1);
			x_off++;
			y_off++;
			down_stroke(buf, 1);
			x_off++;
			y_off++;
			down_stroke(buf, 3);
			x_off++;
			y_off--;
			down_stroke(buf, 1);
			x_off++;
			y_off--;
			down_stroke(buf, 1);
			x_off++;
			break;

		case 'Z':
		case 'z':
			right_stroke(buf, 4);
			x_off += 2;
			y_off++;
			down_stroke(buf, 1);
			x_off--;
			y_off++;
			right_stroke(buf, 1);
			x_off--;
			y_off++;
			down_stroke(buf, 1);
			y_off++;
			right_stroke(buf, 4);
			x_off += 4;
			y_off -= 4;
			break;

		case '\n':
			x_off = -1;
			y_off += 7;
			// no break

		default:
			return "";
		}
		return buf.str();
	}

	std::string str()
	{
		std::ostringstream output_buf(pre_buf);

		while (!char_buf.empty())
		{
			char c = char_buf.front();
			char_buf.pop();

			std::string px_message = get_px_message(c);

			output_buf << px_message;
			x_off += 1;
		}
		return output_buf.str();
	}

	void str(const std::string& init)
	{
		pre_buf = init;
		while (!char_buf.empty())
			char_buf.pop();
	}

	IPXStream& operator<<(char c)
	{
		char_buf.push(c);
		return *this;
	}

	IPXStream& operator<<(const std::string& str)
	{
		for (char c : str)
		{
			*this << c;
		}
		return *this;
	}

private:
	ssize_t x_off{0};
	ssize_t y_off{0};
	std::queue<char> char_buf;
	std::string pre_buf{""};

	unsigned int scale;

	uint8_t r;
	uint8_t g;
	uint8_t b;
};

int main(int argc, char** argv)
{
	if (argc < 3)
	{
		std::clog << "syntax: topx [scale] [content]\n";
		return 1;
	}

	IPXStream stream(5);
	stream << "Hello world\n";

	std::cout << stream.str() << std::endl;

	return 0;
}
